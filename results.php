<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once '../users/init.php';  //make sure this path is correct!
if (!securePage($_SERVER['PHP_SELF'])){die();}
$logged_in = $user->data();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<link href="https://hullseals.space/favicon.ico" rel="icon" type="image/x-icon">
  <link href="https://hullseals.space/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <meta charset="UTF-8">
  <meta content="David Sangrey" name="author">
  <meta content="hull seals, elite dangerous, distant worlds, seal team fix, mechanics, dw2" name="keywords">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport">
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha384-/LjQZzcpTzaYn7qWqRIWYC5l8FWEZ2bIHIz0D73Uzba4pShEcdLdZyZkI4Kv676E" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" integrity="sha384-1CmrxMRARb6aLqgBO7yyAxTOQE2AKb9GfXnEo760AUcUmFx3ibVJJAzGytlQcNXd" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="../assets/datatables.min.css"/>
    <script type="text/javascript" src="../assets/javascript/datatables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://hullseals.space/assets/css/Centercss.css" />
	<script>
  	$(document).ready(function() {
  	$('#LookupList').DataTable();
		} );
	</script>
	<meta content="Trainer Lookup" name="description">
	<title>Trainer Lookup | The Hull Seals</title>
</head>
</html>
<body>
	<div id="home">
		<header>
			<nav class="navbar container navbar-expand-lg navbar-expand-md navbar-dark" role="navigation">
				<a class="navbar-brand" href="../"><img alt="Logo" class="d-inline-block align-top" height="30" src="../images/emblem_scaled.png"> Hull Seals</a><button aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarNav" data-toggle="collapse" type="button"><span class="navbar-toggler-icon"></span></button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link" href="../">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="../knowledge">Knowledge Base</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="../journal">Journal Reader</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="../about">About</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="../contact">Contact</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="https://hullseals.space/users/">Login/Register</a>
						</li>
					</ul>
				</div>
			</nav>
		</header>
    <section class="introduction container">
      <article>
<?php
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$db = include 'db.php';
$mysqli = new mysqli($db['server'], $db['user'], $db['pass'], $db['db'], $db['port']);
$stmt = $mysqli->prepare("SELECT c.*, ca.dispatch
FROM cases AS c
    INNER JOIN case_assigned AS ca ON ca.case_ID = c.case_ID
    INNER JOIN sealsudb.staff AS ss ON ss.seal_id = ca.seal_kf_id
WHERE seal_name = ?");
$stmt->bind_param("s", $_POST['seal_name']);
$stmt->execute();
$result = $stmt->get_result();
if($result->num_rows === 0) exit('No Rescues');
echo "<h2>Returning all Successful Cases for: ";
echo $_POST['seal_name'];
echo "</h2>";
?>
<br>
<?php
echo '<table border="5" cellspacing="2" cellpadding="2" class="table table-dark table-striped table-bordered table-hover table-sm table-responsive-sm" id="LookupList">
			<thead>
			<tr>
          <th> <font face="Arial">Case ID</font> </th>
          <th> <font face="Arial">Case Date</font> </th>
          <th> <font face="Arial">CMDR Type?</font> </th>
      </tr>
			</thead>';
    while ($row = $result->fetch_assoc()) {
        $field1name = $row["case_ID"];
        $field2name = $row["case_created"];
        $field3name = $row["dispatch"];
        echo '<tr>
                  <td>'.$field1name.'</td>
                  <td>'.$field2name.'</td>
									<td>';
		              if ($field3name == "1") {
		                echo 'Dispatcher';
		              }
		              elseif ($field3name == "0") {
		                echo 'Seal';
		              }
									echo '</td>
		            </tr>';
    }
    echo '</table>';
    $result->free();
?>
<a href="." class="btn btn-success btn-lg active" style="float:right;">Go Back</a><br>
</article>
<div class="clearfix"></div>
</section>
</div>
<?php include '../assets/includes/footer.php'; ?>
</body>
</html>
